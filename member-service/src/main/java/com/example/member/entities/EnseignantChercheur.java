package com.example.member.entities;

import java.util.Collection;
import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@DiscriminatorValue("ens")
@Data
@NoArgsConstructor
public class EnseignantChercheur extends Member {
	
	@NonNull
	private String grade,etablissement;
	@OneToMany(mappedBy = "encadrant")
	private Collection<Etudiant> etudiants;

	public EnseignantChercheur(@NonNull String cin, @NonNull String nom, @NonNull String prenom,
			@NonNull Date dateNaissance, String email, long tel, String photo, @NonNull String cv,
			@NonNull String password, @NonNull String confirmedPassword, @NonNull String grade,
			@NonNull String etablissement) {
		super(cin, nom, prenom, dateNaissance, email, tel, photo, cv, password, confirmedPassword);
		this.grade = grade;
		this.etablissement = etablissement;
	}

	

}
