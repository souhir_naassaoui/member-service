package com.example.member.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_mbr", discriminatorType = DiscriminatorType.STRING, length = 3)
@Data
@NoArgsConstructor
@ToString
public abstract class Member implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NonNull
	private String cin, nom, prenom;
	@NonNull
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	@Column(unique = true)
	private String email;
	private long tel;
	private String photo;
	@NonNull
	private String cv, password, confirmedPassword;
	
	

	public Member(@NonNull String cin, @NonNull String nom, @NonNull String prenom, @NonNull Date dateNaissance,
			String email, long tel, String photo, @NonNull String cv, @NonNull String password,
			@NonNull String confirmedPassword) {
		super();
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.tel = tel;
		this.photo = photo;
		this.cv = cv;
		this.password = password;
		this.confirmedPassword = confirmedPassword;
	}

}
