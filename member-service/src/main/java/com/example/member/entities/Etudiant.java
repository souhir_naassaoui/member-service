package com.example.member.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@DiscriminatorValue("etd")
@Data
@NoArgsConstructor
public class Etudiant extends Member {
	@NonNull
	@Temporal(TemporalType.DATE)
	private Date dateInscription;

	private String diplome;
	@ManyToOne
	@JsonProperty(access = Access.WRITE_ONLY)
	private EnseignantChercheur encadrant;

	public Etudiant(@NonNull String cin, @NonNull String nom, @NonNull String prenom, @NonNull Date dateNaissance,
			String email, long tel, String photo, @NonNull String cv, @NonNull String password,
			@NonNull String confirmedPassword, @NonNull Date dateInscription, String diplome,
			EnseignantChercheur encadrant) {
		super(cin, nom, prenom, dateNaissance, email, tel, photo, cv, password, confirmedPassword);
		this.dateInscription = dateInscription;
		this.diplome = diplome;
		this.encadrant = encadrant;
	}

}
