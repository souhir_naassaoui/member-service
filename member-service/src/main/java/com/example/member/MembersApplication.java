package com.example.member;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.example.member.dao.EnseignantRepository;
import com.example.member.dao.EtudiantRepository;
import com.example.member.entities.EnseignantChercheur;
import com.example.member.entities.Etudiant;

@SpringBootApplication
public class MembersApplication implements CommandLineRunner{
	
	@Autowired
	private EtudiantRepository etudiantRepository;
	@Autowired
	private EnseignantRepository enseignantRepository;

	public static void main(String[] args) {
		SpringApplication.run(MembersApplication.class, args);
	}
	

	@Override
	public void run(String... args) throws Exception {
		DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		EnseignantChercheur m1=enseignantRepository.save(new EnseignantChercheur("123456", "adel", "alimi", df.parse("12/10/1974"),
				"adel@gmail", 29786412, "adel.jpg", "cv.pdf", "123", "123", "prof", "ENIS"));
		EnseignantChercheur m2=enseignantRepository.save(new EnseignantChercheur("120006", "slim", "kanoun", df.parse("12/10/1964"),
				"slim@gmail", 29000412, "slim.jpg", "cv.pdf", "123", "123", "prof", "ENIS"));
		etudiantRepository.save(new Etudiant("12365", "souhir", "naass", df.parse("05/07/1998"),"sou@gmail",
				2977788, "sou.jpg", "cv.pdf", "1234", "1234", new Date(), "ing", m1));
		etudiantRepository.save(new Etudiant("12365", "imene", "naass", df.parse("05/07/1998"),"imene@gmail",
				2977788, "sou.jpg", "cv.pdf", "1234", "1234", new Date(), "ing", m2));
		etudiantRepository.save(new Etudiant("12365", "najwa", "naass", df.parse("05/07/1998"),"najwa@gmail",
				2977788, "sou.jpg", "cv.pdf", "1234", "1234", new Date(), "ing", m1));
		System.out.println("**************");
		enseignantRepository.findAll().forEach(c->{
			System.out.println(c.getNom());
		});
		System.out.println("**************");
		etudiantRepository.findAll().forEach(c->{
			System.out.println(c.getNom());
		});
	}

}
