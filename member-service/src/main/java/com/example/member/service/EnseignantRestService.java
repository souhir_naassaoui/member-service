package com.example.member.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.member.dao.EnseignantRepository;
import com.example.member.dao.EtudiantRepository;
import com.example.member.entities.EnseignantChercheur;
import com.example.member.entities.Etudiant;


@RestController
@CrossOrigin
public class EnseignantRestService {
	
	@Autowired
	private EnseignantRepository enseignatRepository;
	
	@GetMapping("/enseignants")
	public List<EnseignantChercheur> getContacts(){
		return enseignatRepository.findAll();
	}
	
	@GetMapping("/enseignants/{id}")
	public Optional<EnseignantChercheur> getContact(@PathVariable Long id) {
		return enseignatRepository.findById(id);
	}
	
	@DeleteMapping("/enseignants/{id}")
	public void deleteContact(@PathVariable Long id) {
		enseignatRepository.deleteById(id);
	}
	
	@PostMapping("/enseignant")
	public EnseignantChercheur addContact(@RequestBody EnseignantChercheur contact){
		return enseignatRepository.save(contact);
	}
	
	@PutMapping("/enseignant/{id}")
	public EnseignantChercheur updateContact(@PathVariable Long id, @RequestBody EnseignantChercheur contact) {
		contact.setId(id);
		return enseignatRepository.save(contact);
	}
	@GetMapping("/chercherEnseignants")
	public Page<EnseignantChercheur> chercher( @RequestParam(name = "mc", defaultValue = "") String mc,
			@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "5") int size){
		return enseignatRepository.chercher("%"+mc+"%", PageRequest.of(page, size));
	}

}
