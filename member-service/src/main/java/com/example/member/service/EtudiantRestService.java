package com.example.member.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.member.dao.EtudiantRepository;
import com.example.member.entities.Etudiant;


@RestController
@CrossOrigin
public class EtudiantRestService {
	
	@Autowired
	private EtudiantRepository etudiantRepository;
	
	@GetMapping("/etudiants")
	public List<Etudiant> getContacts(){
		return etudiantRepository.findAll();
	}
	
	@GetMapping("/etudiants/{id}")
	public Optional<Etudiant> getContact(@PathVariable Long id) {
		return etudiantRepository.findById(id);
	}
	
	@DeleteMapping("/etudiants/{id}")
	public void deleteContact(@PathVariable Long id) {
		etudiantRepository.deleteById(id);
	}
	
	@PostMapping("/etudiant")
	public Etudiant addContact(@RequestBody Etudiant contact){
		return etudiantRepository.save(contact);
	}
	
	@PutMapping("/etudiant/{id}")
	public Etudiant updateContact(@PathVariable Long id, @RequestBody Etudiant contact) {
		contact.setId(id);
		return etudiantRepository.save(contact);
	}
	@GetMapping("/chercherEtudiants")
	public Page<Etudiant> chercher( @RequestParam(name = "mc", defaultValue = "") String mc,
			@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "2") int size){
		return etudiantRepository.chercher("%"+mc+"%", PageRequest.of(page, size));
	}

}
