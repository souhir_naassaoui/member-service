package com.example.member.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.member.entities.EnseignantChercheur;

public interface EnseignantRepository extends JpaRepository<EnseignantChercheur, Long>{
	
	@Query("select c from EnseignantChercheur c where c.nom like :x")
	public Page<EnseignantChercheur> chercher(@Param("x") String mc, Pageable pageable);
	
	List<EnseignantChercheur>findByGrade(String grade);
	List<EnseignantChercheur>findByEtablissement(String etablissement);

}
