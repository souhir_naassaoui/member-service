package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApplication {
	
	/*
	 * @Bean
	RouteLocator routes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(r->r.path("/salat/**")
						.filters(f->f
								.addRequestHeader("x-rapidapi-host", "muslimsalat.p.rapidapi.com")
								.addRequestHeader("x-rapidapi-key", "8e2ba70204msh4a61edb559b3c54p161464jsn72b52c2bc21b")
								.rewritePath("/salat/(?<segment>.*)", "/${segment}")
								
						)						
						.uri("https://muslimsalat.p.rapidapi.com"))
				//.route(r->r.path("/products/**").uri("lb://INVENTORY-SERVICE"))
				.build();
	}
	 */
	@Bean
	DiscoveryClientRouteDefinitionLocator dynamicRoutes(ReactiveDiscoveryClient rdc, DiscoveryLocatorProperties dlp) {
		return new DiscoveryClientRouteDefinitionLocator(rdc, dlp);
	}

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

}
