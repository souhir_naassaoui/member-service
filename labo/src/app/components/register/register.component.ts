import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MemberService } from 'src/app/member.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  memberFormGroup!:FormGroup;
  submitted:boolean=false;
  errorMessage!:string;
  successMessage!:string;
  constructor(private  fb:FormBuilder, private productService:MemberService,
              private router:Router) { }

  ngOnInit(): void {
    this.memberFormGroup=this.fb.group({
      name:["",Validators.required],
      password:["",Validators.required],
      confirmedPassword:["",Validators.required],
  
    });
  }

  onSave() {
    this.submitted=true;
    if(this.memberFormGroup.invalid) return;
    this.productService.save(this.memberFormGroup.value).subscribe(data=>{
      //console.log(data);
      this.successMessage="Inscription réussite ! Merci de se connecter."
      this.router.navigateByUrl('/Login');
    },err=>{
      this.errorMessage="Merci de vérifier vos coordonnées."
      console.log(err);
    });
  }
}
