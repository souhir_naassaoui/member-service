import { Component, OnInit } from '@angular/core';
import { AuthenticationServiceService } from 'src/app/authentication-service.service';
import { MemberService } from 'src/app/member.service';

@Component({
  selector: 'app-etudiants',
  templateUrl: './etudiants.component.html',
  styleUrls: ['./etudiants.component.css']
})
export class EtudiantsComponent implements OnInit {

  constructor(private memberService:MemberService,private authService:AuthenticationServiceService) { }
  members:any;

  ngOnInit(): void {
    this.memberService.getAllStudents().subscribe(data=>{
      this.members=data;
    },err=>{
      console.log(err);
    })
  }
  onDelete(id:any){
    let v=confirm('Etes-vous sure de vouloir supprimer ');
    if(v)
    this.memberService.deleteMember(id).subscribe(data=>{
      this.onGetAllStudents();
    });
  }
 
  onGetAllStudents(){
    this.memberService.getAllStudents().subscribe(data=>{
      this.members=data;
    },err=>{
      console.log(err);
    })
  }
  onEdit(member:any){

  }
  isAuthenticated(){
    return this.authService.isAuthenticated();
  }
  isAdmin(){
    return this.authService.isAdmin();
  }

}
