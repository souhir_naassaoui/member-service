import { Component, OnInit } from '@angular/core';
import { MemberService } from 'src/app/member.service';

@Component({
  selector: 'app-enseignants',
  templateUrl: './enseignants.component.html',
  styleUrls: ['./enseignants.component.css']
})
export class EnseignantsComponent implements OnInit {

  constructor(private memberService:MemberService) { }
  members:any;

  ngOnInit(): void {
    this.memberService.getAllTeachers().subscribe(data=>{
      this.members=data;
    },err=>{
      console.log(err);
    })
  }
}
