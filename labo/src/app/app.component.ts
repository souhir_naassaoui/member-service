import { AuthenticationServiceService } from './authentication-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'labo';
  username:any;
  constructor(private authService:AuthenticationServiceService) { }

  ngOnInit(): void {
      this.authService.loadToken();
  }

   isAdmin(){
    return this.authService.isAdmin();
  }
  isUser(){
    return this.authService.isUser();
  }
  isAuthenticated(){
    return this.authService.isAuthenticated();
  }
  logout(){
    this.authService.logout();
  }
 
}
