import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { EtudiantsComponent } from './components/etudiants/etudiants.component';
import { EnseignantsComponent } from './components/enseignants/enseignants.component';
import { EditMemberComponent } from './components/edit-member/edit-member.component';


const routes: Routes = [
  {path:'Login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'etudiants',component:EtudiantsComponent},
  {path:'enseignants',component:EnseignantsComponent},
  {path:'edit-profile',component:EditMemberComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
