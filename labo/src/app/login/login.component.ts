import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationServiceService } from '../authentication-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  jwt:any;
  constructor(private authService:AuthenticationServiceService, private route:Router) { }

  ngOnInit(): void {
  }
  onLogin(data:any){
    
    this.authService.login(data).subscribe(resp=>{
      this.jwt=resp.headers.get('Authorization');
      this.authService.saveToken(this.jwt);
      this.route.navigateByUrl('/');
    },err=>{
      
    })
  }

 


}
