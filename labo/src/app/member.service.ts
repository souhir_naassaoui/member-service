import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from './model/Member.model';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  public memberHost:string="http://localhost:8083";
  public host:string="http://localhost:8087";

  constructor(private http:HttpClient) { }

  getAllStudents(){
    return this.http.get(this.memberHost+"/etudiants");
  }
  getAllTeachers(){
    return this.http.get(this.memberHost+"/enseignants");
  }
  save(member:Member):Observable<Member>{
    
    return this.http.post<Member>(this.host+"/register",member);
  }
  deleteMember(id:any):Observable<void>{
    return this.http.delete<void>(this.memberHost+"/etudiants/"+id);
  }
}
